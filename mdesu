#!/bin/sh
homecache="${XDG_CACHE_HOME:-$HOME/.cache}/mdesu"
#userfollows="${homecache}/userfollows.json"
mangafile="${homecache}/follows.txt"
log="${homecache}/log.txt"
api="https://api.mangadex.org"
cachdir="/tmp/mangadex/cache/"
tab=$(printf '\t')
nl=$(printf '\n')
extlinks="${homecache}/extlinks"
taglist="${homecache}/taglist.txt"
pagelimit=100

trap 'cleanup' TERM INT EXIT
trap 'message 1>&2 "$message"' USR1
PROC="$$"

quit(){ kill -TERM $PROC ;}

cleanup(){
	rm -f "$curlfile"
	rm -rf "$tempdir"
	[ -n "$pid" ] && [ -e /proc/"$pid" ] && kill "$pid"
	exit
}

message(){ printf "%s" "$1" ;}

install(){
	mkdir -p "$homecache"
	touch "$mangafile"
	[ -e "$taglist" ] || curl -s ${api}/manga/tag | jq -jr '.[].data|.id, "\t", .attributes.name.en, "\n"' > "$taglist"
	exit
}

spinner(){
	sp='/-\|'
	printf ' '
	while true; do
		printf '\b%.1s' "$sp"
		sp=${sp#?}${sp%???}
		sleep 0.1
	done
}

spinwrap(){
	printf '\r\033[K'
	printf '%s' "$2"
	spinner & pid=$!
	eval "$1"
	kill $pid
	printf '\r\033[K'
}

add_follows(){
	echo "$newfollows" | tee -a "$mangafile" | \
		awk -F'\t' 'BEGIN{print "added to follows:"}{print $1}'
	sort -u -o "$mangafile" "$mangafile"
}
#		-d 'excludedTags[]=b13b2a48-c720-44a9-9c77-39c9979373fb' \
curl_search(){
	printf -- "--data-urlencode title=%s " "$(echo "$search" | tr ' ' '+')"
	#printf -- '-d excludedTags[]=b13b2a48-c720-44a9-9c77-39c9979373fb '
	printf -- '-d limit=100 '
	printf -- '%s/manga' "$api"
}
# ADVANCED SEARCH
include_tags(){
	printf 'Include Tags: '
	{ [ -n "$excluded_tags" ] && included_tags=$(sed -f "$sedscript" "$taglist" | fzf -d'\t' -m --with-nth=2 | cut -f1) ;} || included_tags=$(fzf -d'\t' -m --with-nth=2 < "$taglist" | cut -f1)
	included_tags=$(tag_loop "$included_tags" includedTags)
	rm -f "$sedscript"
}
exclude_tags(){
	printf 'Exclude Tags: '
	excluded_tags=$(fzf -d'\t' -m --with-nth=2 < "$taglist" | cut -f1)
	[ -n "$getintags" ] && make_sedscript
	[ -n "$excluded_tags" ] && excluded_tags=$(tag_loop "$excluded_tags" excludedTags)
}
make_sedscript(){
	sedscript=$(mktemp)
	while read -r line;do
		printf '/%s/d\n' "$line"
	done <<- EOF > "$sedscript"
	$excluded_tags
	EOF
}
tag_loop(){
	while read -r line;do
		printf '%s[]=%s&' "$2" "$line"
	done <<- EOF
	$1
	EOF
}
content_rating(){
	printf 'multiple select to include: '
	contentrating=$(printf 'safe\nsuggestive\nerotica\npornographic' | fzf -m)
	contentrating=$(tag_loop "$contentrating" contentRating)
}
demographics(){
	printf 'select demographics: '
	demo=$(printf 'shounen\nshoujo\njosei\nseinen\nnone' | fzf -m)
	demo=$(tag_loop "$demo" publicationDemographic)
}
adv_curl_search(){
	[ -n "$search" ] && printf -- "--data-urlencode title=%s " "$(echo "$search" | tr ' ' '+')"
	[ -n "$included_tags" ] && printf -- '-d %s ' "${included_tags%&}"
	[ -n "$excluded_tags" ] && printf -- '-d %s ' "${excluded_tags%&}"
	[ -n "$contentrating" ] && printf -- '-d %s ' "${contentrating%&}"
	[ -n "$demo" ] && printf -- '-d %s ' "${demo%&}"
	printf -- '-d limit=100 '
	printf -- '%s/manga' "$api"
}
get_title(){ printf 'Title search: '; read -r search ;}
adv_search(){
	searchcat=$(cat <<- EOF
	title
	include tags
	exclude tags
	pages
	demographics
	content rating -- pornography excluded by default
	EOF
	)
	printf 'Search Methods:'
	while read -r line;do
		case "$line" in
			title) gettitle="yes" ;;
			exclude\ tags)getextags="yes";;
			include\ tags)getintags="yes";;
			demographics)getdemographics="yes";;
			content\ rating*)getrating="yes";;
			pages)getpages="yes" ;;
		esac
	done <<- EOF
	$(echo "$searchcat" | fzf -m --cycle)
	EOF
	printf '\r\033[K'
	[ "$gettitle" ] && get_title
	[ "$getextags" ] && exclude_tags
	[ "$getintags" ] && include_tags
	[ "$getrating" ] && content_rating
	[ "$getdemographics" ] && demographics
	[ "$getpages" ] && { printf 'How many pages?'; pages="$( seq "$pagelimit"| fzf --cycle )" ;}
	[ -n "$gettitle$getintags$getextags$getrating$getdemographics" ] || { echo "select a method"; exit ;}
	tempfile=$(mktemp -t search.XXXXXXXX)
	# shellcheck disable=SC2046
	curl -s -G $(adv_curl_search) > "$tempfile"
	[ "${pages:-1}" -gt 1 ] && spinwrap "many_pages" "fetching $pages pages..."
	search_results
}
many_pages(){
	limit=100
	offset=0
	tempsearch1=$(mktemp)
	tempsearch2=$(mktemp)
	advtotal=$(jq '.total' "$tempfile")
	advans=$(((advtotal+(limit -1))/limit -1))
	[ "$pages" = 100 ] && pages=99
	[ "$pages" -gt "$advans" ] && pages="$advans"
	while [ "$offset" -lt "$pages" ]; do
		offset=$((offset + 1))
		cat "$tempfile" > "$tempsearch1"
		# shellcheck disable=SC2046
		curl -s -G $(adv_curl_search) > "$tempsearch2"
		jq -nrc '{ results: [ inputs.results ] | add }' "$tempsearch1" "$tempsearch2" > "$tempfile"
	done
	rm "$tempsearch1" "$tempsearch2"
}
# END ADVANCED SEARCH
search(){
	printf "Search Mangadex: "
	read -r search
	[ -n "$search" ] || quit
	tempfile=$(mktemp -t search.XXXXXXXX)
	# shellcheck disable=SC2046
	curl -s -G $(curl_search) > "$tempfile"
	search_results
}
search_results(){
#	always backup $mangafile before messing with this
	newfollows=$(jq -jr '.results[].data|.attributes.title.en, "\t", .id, "\n"' "$tempfile" | \
		fzf -m -d'\t' --with-nth=1 --preview-window=wrap --preview "var={};jq -r --arg a \"\${var%%$tab*}\" '.results[].data.attributes|select(.title.en == \$a )|.description.en' \"$tempfile\"")
	rm "$tempfile"
	while [ -n "$newfollows" ];do
		case "$(printf 'add to follows\ngoto chapters\nexit' | fzf --cycle --prompt='Choice: ')" in
			add*) add_follows;exit;;
			goto*) { [ "$(echo "$newfollows"|wc -l)" -gt 1 ] && \
				echo "This feature dosn't work with multiple selections!" ;} || \
				 { mangaId=${newfollows##*$tab};mangatitle=${newfollows%%$tab*};get_chapter ;};;
			exit) exit;;
		esac
	done
}

many_chapters(){
	feedtmp1=$(mktemp -t feedtmp.XXXXXXXX)
	feedtmp2=$(mktemp -t feedtmp.XXXXXXXX)
	while [ "$offset" -lt "$ans" ]; do
		offset=$((offset + 1))
		cat "$cachdir""$mangaId".json > "$feedtmp1"
		curl -s -G \
			-d "manga=$mangaId" \
			-d "limit=100" \
			-d "translatedLanguage[]=en" \
			-d "offset=$((offset*100))" \
			"${api}/chapter" > "$feedtmp2"
		jq -nrc '{ results: [ inputs.results ] | add }' "$feedtmp1" "$feedtmp2" > "$cachdir""$mangaId".json
	done
	rm "$feedtmp1" "$feedtmp2"
}

feed_curl(){
	curl -o "$cachdir""$mangaId".json --create-dirs -s \
		"${api}/chapter?manga=${mangaId}&limit=100&translatedLanguage[]=en&offset=0"
}
chapter_feed(){
	offset=0
	limit=100
	spinwrap "feed_curl" 'fetching chapter list...'
	total=$(jq '.total' "$cachdir""$mangaId".json)
	ans=$(((total+(limit -1))/limit -1))
	[ $offset -lt $ans  ] && spinwrap "many_chapters" "many chapters please wait..."
}

selectch(){
	jq -jr '.results[].data|"ch ", .attributes.chapter, "\t", .attributes.title, "\t", .id, "\t", .attributes.hash, "\t",  (.attributes.data|@sh), "\n"' "$cachdir""$mangaId".json | sed 's/ch null/ch 0/g' | sort -V | fzf ${module:+"-m"} ${tac:+"--tac"} --cycle -d'\t' --with-nth=1,2 --tiebreak=index | cut -f 1,3,4,5 | tr -d \' | head -n60
}

curlformat(){
	for filename in $1; do
		printf "url = \"%s/data/%s/%s\"\n" "$server" "$chapterhash" "$filename"
		printf "output = \"%s/%s.%s\"\n" "$tempdir" "$i" "${filename##*.}"
		_=$((i+=1))
	done
}

clean_log(){
	tmpfile=$(mktemp -t clean.XXXXXXXX)
	sort "$log" | cut -f1 | sort -u | \
		while read -r line; do
	grep "${line}${tab}" "$log" |sort -V | tail -n1
	done > "$tmpfile" && mv "$tmpfile" "$log"
}

help(){
	cat <<- EOF
	Usage: ${0##*/} [OPTIONS]
	Mangadex commandline / fzf interface.
	Needs $mangafile make with -i and populate with -s
	Options -lmr can be combined, everything else does it's thing and exits.

	  -s	search mangadex by title, excludes doujinshi, use advanced search for those
	  -S	advanced search, any combination of title, excluded or included tags
	  -c	select a file to print
	  -e	edit follows list
	  -E	edit this script
	  -r	reverse chapter order
	  -l	after choosing manga, loop through chapter selection
	  -m	multi chapter selection. for manga with low page to chapter ratio. 60 chapters max
	  -d	purge mangadex folder from /tmp
	  -i	install. makes $mangafile
	  -h	this help
	EOF
}

cat_files(){
	case "$(printf 'follows\nlog\nexternal links\nscript\n' | fzf --cycle --prompt='get filepath:')" in
		follows)echo "$mangafile";;
		log)echo "$log";;
		external*)echo "$extlinks";;
		script)echo "$0";;
	esac
}

chapter_test(){ chapterTest="${chapterdata%% *}"; [ "${chapterTest#*http}" != "$chapterTest" ] ;}
get_server(){ curl -s "${api}/at-home/server/${chapterId}" | jq -r .baseUrl ;}

single_IFS(){
	i=1
	IFS=$tab read -r chapter chapterId chapterhash chapterdata <<- EOF
	$choice
	EOF
	chapter_test && message="external link: $chapterTest" && kill -USR1 $PROC && quit
	printf '%s\tch %s\n' "$mangatitle" "$chapter" >> "$log"
	server=$(get_server)
	curlformat "$chapterdata"
}

multi_IFS(){
	i=1
	while IFS=$nl read -r chunk;do
		while IFS=$tab read -r chapter chapterId chapterhash chapterdata;do
			chapter_test && echo "$chapterTest" >> "$extlinks" && continue
			printf '%s\t%s\n' "$mangatitle" "$chapter" >> "$log"
			server=$(get_server)
			curlformat "$chapterdata"
		done <<- EOF
		$chunk
		EOF
	done <<- EOF
	$choice
	EOF
}

single_mkdir(){
	choice=${choice#ch }
	[ -n "${choice%%$tab*}" ] || exit
	tempdir="/tmp/mangadex/$mangatitle/chapter_${choice%%$tab*}"
	mkdir -p "$tempdir"
}

multi_mkdir(){
	mkdir -p /tmp/mangadex
	tempdir=$(mktemp -d -p /tmp/mangadex)
}

get_chapter(){
	[ -f "$cachdir""$mangaId".json ] || chapter_feed
	lastlog=$(grep -F "$mangatitle" "$log")
	[ -n "$lastlog" ] && printf 'last chapter: %s' "${lastlog##*$tab}"
	curlfile=$(mktemp -t curlfile.XXXXXXXX)
	choice=$(selectch)
	[ -n "$choice" ] || { rm -f "$curlfile";exit ;}
	"${module:-single}"_mkdir
	spinwrap "${module:-single}_IFS > $curlfile" "formatting request..."
	clean_log
	[ -s "$curlfile" ] || quit
	spinwrap "curl -s -K $curlfile" "downloading $((i - 1)) pages..."
	find "$tempdir"/* | sort -V | sxiv -iq 2> /dev/null
	rm -rf "$tempdir"
	rm -f "$curlfile"
}

get_loop(){ while true;do get_chapter;done ;}

main(){
	#manga=$(fzf -d'\t' --with-nth=1 --preview-window=wrap --preview "var={};jq -r --arg a \"\${var%%$tab*}\" '.results[].data.attributes|select(.title.en == \$a )|.description.en' $userfollows" < "$mangafile" || quit)
	printf "Followed Manga:"
	manga=$(fzf -d'\t' --with-nth=1 < "$mangafile" || quit)
	printf '\r\033[K'
	mangaId=${manga##*$tab}
	mangatitle=${manga%%$tab*}
	${loop:-get_chapter}
}

while getopts "sScEerlmdih" o;do case "${o}" in
	s) search;exit;;
	S) adv_search;exit;;
	c) cat_files;exit;;
	E) $EDITOR "$0";exit;;
	e) $EDITOR "$mangafile";exit;;
	r) tac="yes";;
	l) echo "Ctrl-C or ESC to exit";loop="get_loop";;
	m) module="multi";;
	d) rm -rf /tmp/mangadex;exit;;
	i) echo "setting up" ;install ;;
	*) help;exit;;
esac done
main
