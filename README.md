# mdesu mangadex fzf commandline
# **this branch is broken use experimental or realtime branch!!**
**for Linux**

depends on fzf curl jq and sxiv

sxiv could be easily substituted by any imageviewer that can take stdin

stuff related to files might be somewhat jank

running ```mdesu``` without options shows followed manga in fzf

run  ```mdesu -i``` to make the required $HOME/.cache/mdesu dir, follows.txt and taglist.txt

search ```mdesu -s```
excludes doujinshi by default to avoid lewding komi, use ```mdesu -S```  to search for doujinshi

NEW! ```mdesu -S``` advanced search, any combination of title, excluded or included tags.
both search methods are currently limited to 100 results

userfollows variable not implemented. needs a json file with manga description at .results[].data.attributes.description.en eg json from /user/follows/manga/feed api endpoint

---
![preview gif](preview.gif)
---
TODO
* [X] search by tag
* [X] more search results (experimental branch)
* [ ] covers. uberzug maybe?
* [ ] log in and get user manga list
* [ ] descriptions for follows.txt
* [X] group names next to chapter (experimental branch)
* [ ] download option (can already do this manually by copying chapter from /tmp/mangadex before closing sxiv)
---
[mangadex api docs](https://api.mangadex.org/docs.html)
